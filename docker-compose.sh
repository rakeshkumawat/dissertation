#!/bin/bash

sudo echo "Creating required directories .... "
sudo mkdir -p /home/ec2-user/dissertation/kafka/data/{kafka-1,kafka-2,kafka-3,zookeeper-1}

#Launch Jupyter Notebook Container
#sudo docker-compose -f /home/ec2-user/dissertation/jupyter/docker-compose.yaml build
#sudo docker-compose -f /home/ec2-user/dissertation/jupyter/docker-compose.yaml up -d

#Launch Apache Hadoop Cluster Containers
sudo docker-compose -f /home/ec2-user/dissertation/docker-hadoop-spark/docker-compose.yml build
sudo docker-compose -f /home/ec2-user/dissertation/docker-hadoop-spark/docker-compose.yml up -d

#Launch Apache Kafka Cluster Containers
sudo docker-compose -f /home/ec2-user/dissertation/kafka/docker-compose.yaml build
sudo docker-compose -f /home/ec2-user/dissertation/kafka/docker-compose.yaml up -d

#sudo docker exec -it zookeeper-1 bash
docker exec kafka bash -c 'apt-get update'
docker exec kafka bash -c 'apt-get install -y python3-pip'
#docker exec zookeeper-1 bash -c '/kafka/bin/kafka-topics.sh --create --zookeeper zookeeper-1:2181 --replication-factor 1 --partitions 3 --topic video_stream_comments'
#docker exec zookeeper-1 bash -c '/kafka/bin/kafka-topics.sh --describe --topic video_stream_comments --zookeeper zookeeper-1:2181'
docker exec kafka bash -c 'kafka-topics --create --bootstrap-server kafka:9092 --replication-factor 1 --partitions 3 --topic mydata-youtube'
#docker exec kafka bash -c 'kafka-topics --describe --topic mydata-youtube --bootstrap-server kafka:9092'

#mkdir -p /home/ec2-user/dissertation/nlp_workflow
#cd /home/ec2-user/dissertation/nlp_workflow
pip3 install notebook
pip3 install pyspark pandas nltk scikit-learn pyspellchecker matplotlib seaborn wordcloud textblob dataprep pdfkit



