import subprocess

# Video ID for the YouTube video
video_id = "Dg2ffigGcYM"
#video_id = "Hgj1byn08lM"


# Check if pytchat is installed
try:
    import pytchat
except ImportError:
    print("pytchat is not installed. Installing now...")
    # Install pytchat using pip3
    subprocess.run(["pip3", "install", "pytchat"])

# Import pytchat after installation
import pytchat as chat

# Create pytchat instance
chat_instance = chat.create(video_id=video_id)

# Open the file in append mode
with open("/kafka-scripts/conference_comments/chat-data.csv", "a") as file:
    # Write the header to the file
    file.write("Timestamp,Author_Name,Message\n")
    
    try:
        # Run the chat loop
        while chat_instance.is_alive():
            for comment in chat_instance.get().sync_items():
                # Print the comment to console
                print(f"{comment.datetime} [{comment.author.name}] - {comment.message}")
                
                # Save the comment to the file
                file.write(f"{comment.datetime},{comment.author.name},{comment.message}\n")
                
                # Flush the data to the file immediately
                file.flush()

    except KeyboardInterrupt:
        pass  # This will catch the Ctrl+C interruption

    finally:
        print("Stopping the chat stream.")
        chat_instance.terminate()  # Close the chat to release resources
