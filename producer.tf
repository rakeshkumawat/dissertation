provider "aws" {
  region  = "ap-south-1"
  profile = "default"
}

resource "aws_instance" "Kafka_Cluster" {
#resource "aws_instance" "workstation-producer" {
  ami           = "ami-08fe36427228eddc4"
  #instance_type = "t2.micro"
  #instance_type = "t2.small"
  #instance_type = "t2.medium"
  instance_type = "t2.large"
  #instance_type = "t2.xlarge"
  key_name      = "my-dessertation-project1"
  tags = {
    Name = "Kafka_Cluster"
  }

  root_block_device {
    volume_size = 40  # Specify the desired storage size in GB
  }

  provisioner "remote-exec" {
    inline = [
      "sudo yum install -y https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm",
      "sudo yum install git ansible -y",
      "sudo git clone https://gitlab.com/rakeshkumawat/dissertation.git",
      "sudo ls",
      "pwd",
      #"sudo yum install -y epel-release",
      #"sudo yum install -y ansible",
      #"sudo pip3 install ansible",
      #"sudo cd /home/ec2-user/dissertation/",
      "cd /home/ec2-user/dissertation/",
      #"sudo git clone https://github.com/jay2tinku/docker-hadoop.git",
      "sudo chmod +x /home/ec2-user/dissertation/docker-compose.sh",
      #"cd /home/ec2-user/dissertation/",
      #"pwd",
      #"whoami",
      #"sudo ifconfig eth0",
      #"hostname",
      "ansible-playbook /home/ec2-user/dissertation/install_n_run_docker_container.yml",
      "sudo docker ps -a",
      "sudo docker exec kafka bash -c 'kafka-topics --describe --topic mydata-youtube --bootstrap-server kafka:9092'",
      "sudo docker exec kafka bash -c 'mkdir -p /kafka-scripts/conference_comments/'",
      "sudo docker exec kafka bash -c 'nohup python3 /kafka-scripts/get_video_data1.py > /tmp/output.log 2>&1 &'",
      "sudo docker exec kafka bash -c 'python3 --version'",
      "sudo sleep 30",
      "sudo docker cp /home/ec2-user/dissertation/conference_comments/chat-data.csv datanode:/",
      "sudo docker exec datanode bash -c 'hadoop fs -put /chat-data.csv /'",
      "sudo docker exec datanode bash -c 'hadoop fs -ls /'",
      "nohup jupyter notebook --ip=0.0.0.0 --port=8888 > /tmp/jupyter.log 2>&1 &",
      "ps -eaf |grep jupyter",
      "ls -l /tmp/jupyter.log",
      "tail -n 10 /tmp/jupyter.log",
      "cat /tmp/jupyter.log",
    ]

    connection {
      type        = "ssh"
      user        = "ec2-user"
      private_key = file("my-dessertation-project1.pem")
      host        = self.public_ip
    }
  }
  provisioner "remote-exec" {
    inline = [
      "sudo chown -R ec2-user /home/ec2-user/dissertation/nlp_workflow/",
      "jupyter notebook list",
    ]

    connection {
      type        = "ssh"
      user        = "ec2-user"
      private_key = file("my-dessertation-project1.pem")
      host        = self.public_ip
    }
  }
}


output "public_ip" {
  value = aws_instance.Kafka_Cluster.public_ip
}


